package com.agiletestingalliance;
import org.junit.Assert;
import org.junit.Test;
import com.agiletestingalliance.TestString;
public class TestOfTest {
 
    @Test
    public void testWhenStringIsNull()  {
        TestString test = new TestString();
        Assert.assertNull(test.gstr());
    }
 
    @Test
    public void testWhenStringIsNotNull()  {
        TestString test = new TestString("NotNull");
	Assert.assertEquals("NotNull",test.gstr());
    }
 
    @Test
    public void testWhenStringEmpty()  {
    	TestString test = new TestString("");
	Assert.assertEquals("",test.gstr());
    }
 
    @Test
    public void testWhenStringUpdated()  {
        TestString test = new TestString("");
	test.string="Hello There";
        Assert.assertEquals("Hello There",test.gstr());
    }
   
}
 

